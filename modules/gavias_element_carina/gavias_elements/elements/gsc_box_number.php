<?php 
namespace Drupal\gavias_blockbuilder\shortcodes;
if(!class_exists('gsc_box_number')):
   class gsc_box_number{
      public function render_form(){
         return array(
           'type'          => 'gsc_box_number',
            'title'        => t('Box number'),
            'size'         => 3,
            'fields' => array(
               array(
                  'id'        => 'title',
                  'type'      => 'text',
                  'title'     => t('Title'),
                  'class'     => 'display-admin'
               ),
               array(
                  'id'        => 'content',
                  'type'      => 'textarea',
                  'title'     => t('Content'),
                  'desc'      => t('Content for box color'),
               ),
               array(
                  'id'        => 'icon',
                  'type'      => 'text',
                  'title'     => t('Icon class'),
                  'std'       => '',
                  'desc'     => t('Use class icon font <a target="_blank" href="http://fontawesome.io/icons/">Icon Awesome</a> or <a target="_blank" href="'.base_path().drupal_get_path('theme', 'gavias_carina').'/demo-font/index.html'.'">Custom icon</a>'),
               ),
               array(
                  'id'        => 'number',
                  'type'      => 'text',
                  'title'     => t('Number'),
                  'desc'      => t('Number display, e.g: 1, 2'),
               ),
               array(
                  'id'        => 'link',
                  'type'      => 'text',
                  'title'     => t('Link'),
               ),
               array(
                  'id'        => 'target',
                  'type'      => 'select',
                  'title'     => t('Open in new window'),
                  'desc'      => t('Adds a target="_blank" attribute to the link'),
                  'options'   => array( 'off' => 'No', 'on' => 'Yes' ),
                  'std'       => 'on'
               ),
               array(
                  'id'        => 'style',
                  'type'      => 'select',
                  'title'     => t('Style'),
                  'options'   => array( 
                     'style-1' => 'Style #1: Number top center, hidden icon',
                     'style-2' => 'Style #2: Number left, show icon',
                     'style-3' => 'Style #3: Icon top, background',
                  )
               ),
               array(
                  'id'        => 'background',
                  'type'      => 'text',
                  'title'     => t('Background box (use for style #3)'),
                  'desc'      => 'e.g: #22DCCE'
               ),
               array(
                  'id'        => 'animate',
                  'type'      => 'select',
                  'title'     => t('Animation'),
                  'sub_desc'  => t('Entrance animation'),
                  'options'   => gavias_blockbuilder_animate(),
               ),
               array(
                  'id'        => 'el_class',
                  'type'      => 'text',
                  'title'     => t('Extra class name'),
                  'desc'      => t('Style particular content element differently - add a class name and refer to it in custom CSS.'),
               ),
            ),                                     
         );
      }

      public function render_content( $item ) {
         if( ! key_exists('content', $item['fields']) ) $item['fields']['content'] = '';
            print self::sc_box_number( $item['fields'], $item['fields']['content'] );
      }

      public static function sc_box_number( $attr, $content = null ){
         global $base_url;
         extract(shortcode_atts(array(
            'title'              => '',
            'content'            => '',
            'icon'               => '',
            'number'             => '',
            'link'               => '',
            'target'             => '',
            'style'              => 'style-1',
            'background'         => '',
            'animate'            => '',
            'el_class'           => ''
         ), $attr));

         // target
         if( $target ){
            $target = 'target="_blank"';
         } else {
            $target = false;
         }
         $el_class .= ' ' . $style;
         if($animate){
            $el_class .= ' wow';
            $el_class .= ' '. $animate;
         }
         
         ?>
         <?php ob_start() ?>
         <?php if($style == 'style-1' || $style == 'style-2'){ ?>
            
            <div class="widget gsc-box-number <?php print $el_class ?>">
               <div class="content-inner">
                  <?php if($number){ ?><div class="number"><span><?php print $number; ?></span></div><?php } ?>
                  <div class="content">
                     <div class="title">
                        <?php if($icon){ ?><div class="icon"><span class="<?php print $icon ?>"></span></div><?php } ?>
                        <?php if($link){ ?>
                           <a <?php print $target ?> href="<?php print $link ?>"><?php print $title ?></a>
                        <?php }else{ ?>
                           <a><?php print $title ?></a>
                        <?php } ?>   
                     </div>   
                     <?php if($content){ ?>
                        <div class="desc"><?php print $content ?></div>
                     <?php } ?>
                  </div>
               </div>
            </div>

         <?php }else{ ?>

            <?php 
               $bg = '';
               if($background) $bg = "style=\"background:" . $background . ";\"";
            ?>
            <div class="widget gsc-box-number <?php print $el_class ?>">
               <div class="box-content" <?php print $bg ?>>
                  <?php if($icon){ ?><div class="icon"><span class="<?php print $icon ?>"></span></div><?php } ?>
                  <div class="content-inner">
                     <div class="content-main">
                        <div class="title">
                           <?php if($link){ ?>
                              <a <?php print $target ?> href="<?php print $link ?>"><?php print $title ?></a>
                           <?php }else{ ?>
                              <a><?php print $title ?></a>
                           <?php } ?>   
                        </div>   
                        <?php if($content){ ?>
                           <div class="desc"><?php print $content ?></div>
                        <?php } ?>
                     </div>
                     <?php if($number){ ?><div class="number"><span><?php print $number; ?></span></div><?php } ?>
                  </div>
               </div>
            </div>
               
         <?php } ?>   
         <?php return ob_get_clean() ?>
      <?php
      } 

      public function load_shortcode(){
         add_shortcode( 'box_number', array($this, 'sc_box_number'));
      }
   }
endif;   
